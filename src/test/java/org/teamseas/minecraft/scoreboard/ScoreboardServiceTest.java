package org.teamseas.minecraft.scoreboard;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import com.google.common.collect.ImmutableList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.teamseas.minecraft.contest.Contest;
import org.teamseas.minecraft.contest.ContestConfig;
import org.teamseas.minecraft.score.ScoreRepository;

@RunWith(MockitoJUnitRunner.class)
public class ScoreboardServiceTest {

  private MockedStatic<Bukkit> bukkit;

  @Mock
  private ScoreRepository scoreRepository;

  @Mock
  private ScoreboardRepository scoreboardRepository;

  @Mock
  private ContestConfig contestConfig;

  private OfflinePlayer currentPlayer;

  private ScoreboardService scoreboard;

  @Before
  public void setUp() throws Exception {
    bukkit = Mockito.mockStatic(Bukkit.class);
    currentPlayer = player("42", "me");
    scoreboard = new ScoreboardService(scoreboardRepository, scoreRepository, contestConfig);
  }

  @After
  public void tearDown() throws Exception {
    bukkit.close();
  }

  @Test
  public void initialScoreboard() {
    List<String> actual = removeCountdown(scoreboard.getLines(currentPlayer));
    List<String> expected = ImmutableList.of(
        "§dLet's save our oceans",
        "",
        "§aTop plastic removers:",
        "§aTotal plastic removed: §60",
        "",
        "§dteamseas.org");
    assertThat(actual, is(expected));
  }

  @Test
  public void toplistScoreboard() {
    Player player1 = player("1", "foo");
    Player player2 = player("2", "bar");
    Player player3 = player("3", "baz");
    List<OfflinePlayer> topPlayers = ImmutableList.of(player1, player2, player3);
    when(scoreRepository.topPlayers(Contest.ALLTIME, 10)).thenReturn(topPlayers);
    when(scoreRepository.getRank(Contest.ALLTIME, player1)).thenReturn(1);
    when(scoreRepository.getRank(Contest.ALLTIME, player2)).thenReturn(2);
    when(scoreRepository.getRank(Contest.ALLTIME, player3)).thenReturn(3);
    when(scoreRepository.getRank(Contest.ALLTIME, currentPlayer)).thenReturn(42);
    when(scoreRepository.getScore(Contest.ALLTIME, player1)).thenReturn(3500);
    when(scoreRepository.getScore(Contest.ALLTIME, player2)).thenReturn(240);
    when(scoreRepository.getScore(Contest.ALLTIME, player3)).thenReturn(13);
    when(scoreRepository.getScore(Contest.ALLTIME, currentPlayer)).thenReturn(5);
    when(scoreRepository.getTotalScore(Contest.ALLTIME)).thenReturn(4711);
    when(contestConfig.getPrize(Contest.ALLTIME, 1, 3)).thenReturn(Optional.of("gold"));
    when(contestConfig.getPrize(Contest.ALLTIME, 2, 3)).thenReturn(Optional.of("silver"));
    List<String> actual = removeCountdown(scoreboard.getLines(currentPlayer));
    List<String> expected = ImmutableList.of(
        "§dLet's save our oceans",
        "",
        "§aTop plastic removers:",
        "§6#1 §bfoo §b(§63.5K§b) - §6gold",
        "§6#2 §bbar §b(§6240§b) - §6silver",
        "§6#3 §bbaz §b(§613§b)",
        "§6#42 §bme §b(§65§b)",
        "§aTotal plastic removed: §64.7K",
        "",
        "§dteamseas.org");
    assertThat(actual, is(expected));
  }

  private ImmutableList<String> removeCountdown(List<String> lines) {
    return lines.stream()
        .filter(line -> !line.contains("left"))
        .collect(ImmutableList.toImmutableList());
  }

  private Player player(String id, String name) {
    Player player = Mockito.mock(Player.class);
    UUID uuid = UUID.fromString(id + "-1-2-3-4");
    // Mockito.when(player.getUniqueId()).thenReturn(uuid);
    Mockito.when(player.getName()).thenReturn(name);
    bukkit.when(() -> Bukkit.getOfflinePlayer(uuid)).thenReturn(player);
    bukkit.when(() -> Bukkit.getOfflinePlayer(name)).thenReturn(player);
    return player;
  }
}
