package org.teamseas.minecraft.scoreboard;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Optional;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.teamseas.minecraft.contest.Contest;
import redis.clients.jedis.Jedis;
import redis.embedded.RedisServer;

public class JedisScoreboardRepositoryTest {

  private MockedStatic<Bukkit> bukkit;
  private RedisServer server;
  private Jedis jedis;
  private ScoreboardRepository repository;

  @Before
  public void setUp() throws Exception {
    bukkit = Mockito.mockStatic(Bukkit.class);
    server = new RedisServer();
    server.start();
    jedis = new Jedis();
    repository = new JedisScoreboardRepository(jedis);
  }

  @After
  public void tearDown() throws Exception {
    server.stop();
    bukkit.close();
  }

  @Test
  public void initialVisibility() {
    Player player = player("1-2-3-4-5");
    assertFalse(repository.isVisible(player));
  }

  @Test
  public void storesVisibility() {
    Player player = player("1-2-3-4-5");
    repository.setVisible(player, true);
    assertTrue(repository.isVisible(player));
  }

  @Test
  public void initialContest() {
    Player player = player("1-2-3-4-5");
    assertEquals(Optional.empty(), repository.getContest(player));
  }

  @Test
  public void storesContest() {
    Player player = player("1-2-3-4-5");
    repository.setContest(player, Contest.DAILY);
    assertEquals(Contest.DAILY, repository.getContest(player).get());
  }

  private Player player(String id) {
    Player player = Mockito.mock(Player.class);
    UUID uuid = UUID.fromString(id);
    Mockito.when(player.getUniqueId()).thenReturn(uuid);
    bukkit.when(() -> Bukkit.getOfflinePlayer(uuid)).thenReturn(player);
    return player;
  }

}
