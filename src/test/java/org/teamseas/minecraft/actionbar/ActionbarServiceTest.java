package org.teamseas.minecraft.actionbar;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.bukkit.entity.Player;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.teamseas.minecraft.contest.Contest;
import org.teamseas.minecraft.score.ScoreRepository;

@RunWith(MockitoJUnitRunner.class)
public class ActionbarServiceTest {

  @Mock
  private Player player;

  @Mock
  private ScoreRepository repository;

  @Test
  public void formatsMessage() {
    when(repository.getScore(Contest.DAILY, player)).thenReturn(1);
    when(repository.getScore(Contest.WEEKLY, player)).thenReturn(2);
    when(repository.getScore(Contest.MONTHLY, player)).thenReturn(3);
    when(repository.getScore(Contest.ALLTIME, player)).thenReturn(4);
    
    when(repository.getRank(Contest.DAILY, player)).thenReturn(5);
    when(repository.getRank(Contest.WEEKLY, player)).thenReturn(6);
    when(repository.getRank(Contest.MONTHLY, player)).thenReturn(7);
    when(repository.getRank(Contest.ALLTIME, player)).thenReturn(8);
    
    ActionbarService service = new ActionbarService(repository);
    
    String expected = "§a1 (○ #5)§7 | §e2 (△ #6)§7 | §c3 (□ #7)§7 | §44 (#8)";
    String actual = service.getMessage(player);
    
    assertEquals(expected, actual);
  }
}