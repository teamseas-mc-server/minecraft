package org.teamseas.minecraft.plastic;

import static org.junit.Assert.assertEquals;

import java.util.Random;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class RandomCollectionTest {

  private Random random;
  private RandomCollection<String> collection;  

  @Before
  public void setUp() {
    random = Mockito.mock(Random.class);
    collection = new RandomCollection<>(random);
  }

  @Test
  public void selectsRandomItem() {
    collection.add(0.5, "foo");
    collection.add(0.5, "bar");

    Mockito.when(random.nextDouble()).thenReturn(0.7);

    assertEquals("bar", collection.next());
  }
}
