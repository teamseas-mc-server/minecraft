package org.teamseas.minecraft.util;

import static org.junit.Assert.assertEquals;

import java.util.Locale;
import org.junit.Test;

public class IntegerPrinterTest {

  @Test
  public void printsFormattedValue() {
    IntegerPrinter printer = new IntegerPrinter();

    assertEquals("0", printer.print(0, Locale.US));
    assertEquals("1", printer.print(1, Locale.US));
    assertEquals("12", printer.print(12, Locale.US));
    assertEquals("123", printer.print(123, Locale.US));
    assertEquals("1.2K", printer.print(1234, Locale.US));
    assertEquals("12.3K", printer.print(12345, Locale.US));
    assertEquals("123.5K", printer.print(123456, Locale.US));
    assertEquals("1.2M", printer.print(1234567, Locale.US));
    assertEquals("12.3M", printer.print(12345678, Locale.US));
    assertEquals("123.5M", printer.print(123456789, Locale.US));
    assertEquals("1.2B", printer.print(1234567891, Locale.US));
    assertEquals("2.1B", printer.print(Integer.MAX_VALUE, Locale.US));
  }

}
