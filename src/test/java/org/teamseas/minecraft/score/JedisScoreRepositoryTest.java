package org.teamseas.minecraft.score;

import static org.junit.Assert.assertEquals;

import com.google.common.collect.ImmutableList;
import java.util.List;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.teamseas.minecraft.contest.Contest;
import redis.clients.jedis.Jedis;
import redis.embedded.RedisServer;

public class JedisScoreRepositoryTest {

  private MockedStatic<Bukkit> bukkit;
  private RedisServer server;
  private Jedis jedis;
  private ScoreRepository repository;

  @Before
  public void setUp() throws Exception {
    bukkit = Mockito.mockStatic(Bukkit.class);
    server = new RedisServer();
    server.start();
    jedis = new Jedis();
    repository = new JedisScoreRepository(jedis);
  }

  @After
  public void tearDown() throws Exception {
    server.stop();
    bukkit.close();
  }

  @Test
  public void initialScore() {
    Player player = player("1-2-3-4-5");
    assertEquals(0, repository.getScore(Contest.ALLTIME, player));
  }

  @Test
  public void initialRank() {
    Player player = player("1-2-3-4-5");
    assertEquals(0, repository.getRank(Contest.ALLTIME, player));
  }

  @Test
  public void initialTotal() {
    assertEquals(0, repository.getTotalScore(Contest.ALLTIME));
  }

  @Test
  public void initialTopPlayers() {
    List<OfflinePlayer> expected = ImmutableList.of();
    List<OfflinePlayer> actual = repository.topPlayers(Contest.ALLTIME, 2);
    assertEquals(expected, actual);
  }

  @Test
  public void playerScore() {
    Player player = player("1-2-3-4-5");
    repository.incrementPlayer(player, 4);
    repository.incrementPlayer(player, -1);
    assertEquals(3, repository.getScore(Contest.ALLTIME, player));
  }

  @Test
  public void totalScore() {
    repository.incrementPlayer(player("1-2-3-4-5"), 20);
    repository.incrementPlayer(player("2-3-4-5-6"), 22);
    assertEquals(42, repository.getTotalScore(Contest.ALLTIME));
  }

  @Test
  public void clearContest() {
    Player player = player("1-2-3-4-5");
    repository.incrementPlayer(player, 4);
    repository.clear(Contest.DAILY);
    assertEquals(0, repository.getScore(Contest.DAILY, player));
    assertEquals(4, repository.getScore(Contest.ALLTIME, player));
  }

  @Test
  public void clearEmptyContest() {
    repository.clear(Contest.DAILY);
  }

  @Test
  public void topPlayers() {
    Player player1 = player("1-2-3-4-5");
    Player player2 = player("2-3-4-5-6");
    Player player3 = player("3-4-5-6-7");
    repository.incrementPlayer(player1, 1);
    repository.incrementPlayer(player2, 2);
    repository.incrementPlayer(player3, 3);
    List<OfflinePlayer> expected = ImmutableList.of(player3, player2);
    List<OfflinePlayer> actual = repository.topPlayers(Contest.ALLTIME, 2);
    assertEquals(expected, actual);
  }

  private Player player(String id) {
    Player player = Mockito.mock(Player.class);
    UUID uuid = UUID.fromString(id);
    Mockito.when(player.getUniqueId()).thenReturn(uuid);
    bukkit.when(() -> Bukkit.getOfflinePlayer(uuid)).thenReturn(player);
    return player;
  }

}
