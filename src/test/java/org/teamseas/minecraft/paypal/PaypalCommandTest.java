package org.teamseas.minecraft.paypal;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.entity.Player;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PaypalCommandTest {

  @Mock
  private Player player;

  @Mock
  private PaypalRepository repository;

  @Mock
  private Command command;

  private static final String LABEL = "unused";

  @Test
  public void initialPaypalEmail() {
    CommandExecutor executor = new PaypalCommand(repository);
    String[] args = {};
    executor.onCommand(player, command, LABEL, args);
    verify(player).sendMessage("§cYou have not set your paypal email.");
  }

  @Test
  public void setsPaypalEmail() {
    CommandExecutor executor = new PaypalCommand(repository);
    String[] args = {"foo"};
    executor.onCommand(player, command, LABEL, args);
    verify(repository).setPaypalEmail(player, "foo");
    verify(player).sendMessage("§aYour paypal email has been set to foo.");
  }

  @Test
  public void getsPaypalEmail() {
    when(repository.getPaypalEmail(player)).thenReturn(Optional.of("foo"));
    CommandExecutor executor = new PaypalCommand(repository);
    String[] args = {};
    executor.onCommand(player, command, LABEL, args);
    verify(player).sendMessage("§aYour paypal email is foo.");
  }

}
