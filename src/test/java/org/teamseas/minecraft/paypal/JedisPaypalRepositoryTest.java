package org.teamseas.minecraft.paypal;

import static org.junit.Assert.assertEquals;

import java.util.Optional;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import redis.clients.jedis.Jedis;
import redis.embedded.RedisServer;

public class JedisPaypalRepositoryTest {
    
  private MockedStatic<Bukkit> bukkit;
  private RedisServer server;
  private Jedis jedis;
  private PaypalRepository repository;

  @Before
  public void setUp() throws Exception {
    bukkit = Mockito.mockStatic(Bukkit.class);
    server = new RedisServer();
    server.start();
    jedis = new Jedis();
    repository = new JedisPaypalRepository(jedis);
  }

  @After
  public void tearDown() throws Exception {
    server.stop();
    bukkit.close();
  }

  @Test
  public void initialEmail() {
    Player player = player("1-2-3-4-5");
    Optional<String> expected = Optional.empty();
    Optional<String> actual = repository.getPaypalEmail(player);
    assertEquals(expected, actual);
  }

  @Test
  public void storeEmail() {
    Player player = player("1-2-3-4-5");
    String email = "example@example.com";
    Optional<String> expected = Optional.of(email);

    repository.setPaypalEmail(player, email);
    
    assertEquals(expected, repository.getPaypalEmail(player));
  }

  private Player player(String id) {
    Player player = Mockito.mock(Player.class);
    UUID uuid = UUID.fromString(id);
    Mockito.when(player.getUniqueId()).thenReturn(uuid);
    bukkit.when(() -> Bukkit.getOfflinePlayer(uuid)).thenReturn(player);
    return player;
  }
}
