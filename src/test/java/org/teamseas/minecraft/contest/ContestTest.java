package org.teamseas.minecraft.contest;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;
import org.junit.Test;

public class ContestTest {

  @Test
  public void endOfDay() {
    LocalDateTime now = LocalDateTime.parse("2021-11-17T12:34:56");
    LocalDateTime expected = LocalDateTime.parse("2021-11-18T00:00:00");
    assertEquals(expected, Contest.DAILY.end(now));
  }

  @Test
  public void endOfWeek() {
    LocalDateTime now = LocalDateTime.parse("2021-11-15T12:34:56");
    LocalDateTime expected = LocalDateTime.parse("2021-11-22T00:00:00");
    assertEquals(expected, Contest.WEEKLY.end(now));
  }

  @Test
  public void endOfMonth() {
    LocalDateTime now = LocalDateTime.parse("2021-11-01T12:34:56");
    LocalDateTime expected = LocalDateTime.parse("2021-12-01T00:00:00");
    assertEquals(expected, Contest.MONTHLY.end(now));
  }

  @Test
  public void endOfAlltime() {
    LocalDateTime now = LocalDateTime.parse("2021-11-17T12:24:56");
    LocalDateTime expected = LocalDateTime.MAX;
    assertEquals(expected, Contest.ALLTIME.end(now));
  }

}
