package org.teamseas.minecraft.contest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

public class DefaultContestConfigTest {

  private ContestConfig config = new DefaultContestConfig();
  
  @Test
  public void dailyPrices() {
    assertFalse(config.getPrize(Contest.DAILY, 1, 10).isPresent());
  }

  @Test
  public void weeklyPrices() {
    assertEquals("$3", config.getPrize(Contest.WEEKLY, 1, 10).get());
    assertEquals("$2", config.getPrize(Contest.WEEKLY, 2, 10).get());
    assertEquals("$1", config.getPrize(Contest.WEEKLY, 3, 10).get());
    assertFalse(config.getPrize(Contest.WEEKLY, 4, 10).isPresent());
  }

  @Test
  public void reducedWeeklyPrices() {
    assertEquals("$2", config.getPrize(Contest.WEEKLY, 1, 2).get());
    assertEquals("$1", config.getPrize(Contest.WEEKLY, 2, 2).get());
    assertFalse(config.getPrize(Contest.WEEKLY, 3, 2).isPresent());
    
    assertEquals("$1", config.getPrize(Contest.WEEKLY, 1, 1).get());
    assertFalse(config.getPrize(Contest.WEEKLY, 2, 1).isPresent());
  }

  @Test
  public void monthlyPrices() {
    assertEquals("$5", config.getPrize(Contest.MONTHLY, 1, 10).get());
    assertEquals("$3", config.getPrize(Contest.MONTHLY, 2, 10).get());
    assertEquals("$2", config.getPrize(Contest.MONTHLY, 3, 10).get());
    assertEquals("$1", config.getPrize(Contest.MONTHLY, 4, 10).get());
    assertFalse(config.getPrize(Contest.MONTHLY, 5, 10).isPresent());
  }

  @Test
  public void reducedMonthlyPrices() {
    assertEquals("$3", config.getPrize(Contest.MONTHLY, 1, 3).get());
    assertEquals("$2", config.getPrize(Contest.MONTHLY, 2, 3).get());
    assertEquals("$1", config.getPrize(Contest.MONTHLY, 3, 3).get());
    assertFalse(config.getPrize(Contest.MONTHLY, 4, 3).isPresent());
    
    assertEquals("$2", config.getPrize(Contest.MONTHLY, 1, 2).get());
    assertEquals("$1", config.getPrize(Contest.MONTHLY, 2, 2).get());
    assertFalse(config.getPrize(Contest.MONTHLY, 3, 2).isPresent());
    
    assertEquals("$1", config.getPrize(Contest.MONTHLY, 1, 1).get());
    assertFalse(config.getPrize(Contest.MONTHLY, 2, 1).isPresent());
  }

  @Test
  public void alltimePrices() {
    assertFalse(config.getPrize(Contest.ALLTIME, 1, 10).isPresent());
  }

}
