package org.teamseas.minecraft.contest;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import com.google.common.collect.ImmutableList;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.teamseas.minecraft.paypal.JedisPaypalRepository;
import org.teamseas.minecraft.paypal.PaypalRepository;
import redis.clients.jedis.Jedis;
import redis.embedded.RedisServer;

@RunWith(Parameterized.class)
public class WinnerCommandTest {

  private final String contest;

  @Parameters
  public static Iterable<String> parameters() {
    return ImmutableList.of("daily", "weekly", "monthly");
  }
  
  public WinnerCommandTest(String contest) {
    this.contest = contest;
  }
  
  private static final String LABEL = "unused";

  @Mock
  private CommandSender sender;
  
  @Mock
  private Command command;

  private MockedStatic<Bukkit> bukkit;
  private RedisServer server;
  private Jedis jedis;
  private PaypalRepository paypalRepository;
  private WinnerRepository winnerRepository;
  private WinnerCommand winnerCommand;

  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.openMocks(this);
    bukkit = Mockito.mockStatic(Bukkit.class);
    server = new RedisServer();
    server.start();
    jedis = new Jedis();
    paypalRepository = new JedisPaypalRepository(jedis);
    winnerRepository = new JedisWinnerRepository(jedis);
    winnerCommand = new WinnerCommand(winnerRepository, paypalRepository);
  }

  @After
  public void tearDown() throws Exception {
    server.stop();
    bukkit.close();
  }

  @Test
  public void listInitialWinners() {
    onCommand("list", contest);
    verify(sender, times(0)).sendMessage(anyString());
  }

  @Test
  public void addWinner() {
    player("1", "foo");
    player("2", "bar");
    onCommand("add", contest, "foo", "gold");
    onCommand("add", contest, "foo", "gold");
    onCommand("add", contest, "foo", "silver");
    onCommand("add", contest, "bar", "bronze");
    onCommand("list", contest);
    verify(sender, times(2)).sendMessage("foo gold ");
    verify(sender).sendMessage("foo silver ");
    verify(sender).sendMessage("bar bronze ");
    verifyNoMoreInteractions(sender);
  }
  
  @Test
  public void removeWinner() {
    player("1", "foo");
    player("2", "bar");
    onCommand("add", contest, "foo", "gold");
    onCommand("add", contest, "foo", "silver");
    onCommand("add", contest, "bar", "silver");
    onCommand("remove", contest, "foo");
    onCommand("list", contest);
    verify(sender).sendMessage("bar silver ");
    verifyNoMoreInteractions(sender);
  }
  
  @Test
  public void listWithPaypalEmail() {
    Player player = player("1", "foo");
    paypalRepository.setPaypalEmail(player, "email");
    onCommand("add", contest, "foo", "gold");
    onCommand("list", contest);
    verify(sender).sendMessage("foo gold email");
    verifyNoMoreInteractions(sender);
  }
  
  private void onCommand(String... args) {
    winnerCommand.onCommand(sender, command, LABEL, args);
  }

  private Player player(String id, String name) {
    Player player = Mockito.mock(Player.class);
    UUID uuid = UUID.fromString(id + "-1-2-3-4");
    Mockito.when(player.getUniqueId()).thenReturn(uuid);
    Mockito.when(player.getName()).thenReturn(name);
    bukkit.when(() -> Bukkit.getOfflinePlayer(uuid)).thenReturn(player);
    bukkit.when(() -> Bukkit.getOfflinePlayer(name)).thenReturn(player);
    return player;
  }
}
