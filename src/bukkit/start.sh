#!/bin/sh
sed "s/\$DISCORD_TOKEN/$DISCORD_TOKEN/" <plugins/EssentialsDiscord/config.yml.in >plugins/EssentialsDiscord/config.yml
java --add-opens java.base/java.lang=ALL-UNNAMED ${AIKAR_FLAGS} -jar core.jar nogui &
trap "mcrcon -s stop ; wait $!" SIGINT SIGQUIT SIGTERM
wait $!