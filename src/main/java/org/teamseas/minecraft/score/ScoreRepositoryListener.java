package org.teamseas.minecraft.score;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.teamseas.minecraft.plastic.PlasticRemovedEvent;

/**
 * Listener that changes score of removed plastic in repository.
 */
public class ScoreRepositoryListener implements Listener {

  private ScoreRepository repository;

  public ScoreRepositoryListener(ScoreRepository repository) {
    this.repository = repository;
  }

  @EventHandler(priority = EventPriority.LOWEST)
  public void onPlasticRemoved(PlasticRemovedEvent event) {
    repository.incrementPlayer(event.getPlayer(), event.getCount());
  }
}
