package org.teamseas.minecraft.score;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import java.util.Locale;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.teamseas.minecraft.contest.Contest;
import org.teamseas.minecraft.util.IntegerPrinter;

/**
 * Displays player information.
 */
public class ScoreCommand implements CommandExecutor {

  private final IntegerPrinter printer = new IntegerPrinter();
  private final ScoreRepository repository;

  public ScoreCommand(ScoreRepository repository) {
    this.repository = repository;
  }

  /**
   * Executed on /score.
   */
  @Override
  public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    if (args.length == 0) {
      Player player = (Player) sender;
      sender.sendMessage(getAllMessages(player));
      return true;
    } 
    
    OfflinePlayer player = Bukkit.getOfflinePlayerIfCached(args[0]);
    
    if (player == null) {
      return false;
    }
    
    if (args.length == 1) {
      sender.sendMessage(getAllMessages(player));
      return true;
    }
    
    final Contest contest;
    
    try {
      contest = Contest.valueOf(args[1]);
    } catch (IllegalArgumentException exception) {
      return false;
    }
    
    if (args.length == 2) {
      sender.sendMessage(getMessage(player, contest));
      return true;
    }

    return false;
  }

  private String getAllMessages(OfflinePlayer player) {
    ImmutableList.Builder<String> messages = ImmutableList.builder();
    for (Contest contest : Contest.values()) {
      messages.add(getMessage(player, contest));
    }
    return Joiner.on('\n').join(messages.build());
  }

  private String getMessage(OfflinePlayer player, Contest contest) {
    int score = repository.getScore(contest, player);
    int rank = repository.getRank(contest, player);

    String message = String.format(
        contest.getColor() + "%s's score is %s and rank is #%s in %s contest.",
        player.getName(), 
        printer.print(score, Locale.US), 
        rank, 
        contest.name());

    return message;
  }
}
