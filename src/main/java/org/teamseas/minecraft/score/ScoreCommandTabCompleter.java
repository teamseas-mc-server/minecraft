package org.teamseas.minecraft.score;

import com.google.common.collect.ImmutableList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.teamseas.minecraft.contest.Contest;

/**
 * Tab completer for score command.
 */
public class ScoreCommandTabCompleter implements TabCompleter {

  @Override
  public List<String> onTabComplete(CommandSender sender, Command command, String alias,
      String[] args) {
    if (args.length == 1) {
      return Stream.of(Bukkit.getOfflinePlayers())
          .map(OfflinePlayer::getName)
          .filter(Objects::nonNull)
          .collect(ImmutableList.toImmutableList());
    }

    if (args.length == 2) {
      return Stream.of(Contest.values())
          .map(Contest::name)
          .collect(ImmutableList.toImmutableList());
    }

    return null;
  }

}
