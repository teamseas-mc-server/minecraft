package org.teamseas.minecraft.score;

import com.google.common.base.Optional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.teamseas.minecraft.contest.Contest;
import redis.clients.jedis.Jedis;

/**
 * Redis implementation of the score repository.
 */
public class JedisScoreRepository implements ScoreRepository {

  private enum Key {
    PLAYER, TOTAL;
  }

  private final Jedis jedis;

  public JedisScoreRepository(Jedis jedis) {
    this.jedis = jedis;
  }

  @Override
  public void incrementPlayer(OfflinePlayer player, int increment) {
    Stream.of(Contest.values()).forEach(contest -> incrementPlayer(contest, player, increment));
  }

  private void incrementPlayer(Contest contest, OfflinePlayer player, int increment) {
    jedis.zincrby(key(contest, Key.PLAYER), increment, member(player));
    jedis.incrBy(key(contest, Key.TOTAL), increment);
  }

  @Override
  public int getTotalScore(Contest contest) {
    String score = jedis.get(key(contest, Key.TOTAL));
    return Integer.parseInt(Optional.fromNullable(score).or("0"));
  }

  @Override
  public int getScore(Contest contest, OfflinePlayer player) {
    Double score = jedis.zscore(key(contest, Key.PLAYER), member(player));
    return Optional.fromNullable(score).or(0.0).intValue();
  }

  @Override
  public int getRank(Contest contest, OfflinePlayer player) {
    Long rank = jedis.zrevrank(key(contest, Key.PLAYER), member(player));
    return rank == null ? 0 : rank.intValue() + 1;
  }

  @Override
  public List<OfflinePlayer> topPlayers(Contest contest, int limit) {
    return jedis.zrevrange(key(contest, Key.PLAYER), 0, limit - 1).stream()
        .map(uuid -> player(uuid))
        .collect(Collectors.toList());
  }

  @Override
  public void clear(Contest contest) {
    renameHistorical(key(contest, Key.PLAYER));
    renameHistorical(key(contest, Key.TOTAL));
  }

  private void renameHistorical(String key) {
    if (jedis.exists(key)) {
      jedis.rename(key, key + ":" + LocalDateTime.now().toString());
    }
  }

  private static String key(Contest contest, Key key) {
    return contest.name() + ":" + key.name();
  }

  private static String member(OfflinePlayer player) {
    return player.getUniqueId().toString();
  }

  private static OfflinePlayer player(String member) {
    return Bukkit.getOfflinePlayer(UUID.fromString(member));
  }

}
