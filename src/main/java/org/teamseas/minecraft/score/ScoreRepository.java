package org.teamseas.minecraft.score;

import java.util.List;
import org.bukkit.OfflinePlayer;
import org.teamseas.minecraft.contest.Contest;

/**
 * Data repository to store plastic removed events per player.
 */
public interface ScoreRepository {

  public void incrementPlayer(OfflinePlayer player, int increment);
  
  public int getTotalScore(Contest contest);

  public int getScore(Contest contest, OfflinePlayer player);
  
  public int getRank(Contest contest, OfflinePlayer player);
  
  public List<OfflinePlayer> topPlayers(Contest contest, int limit);
  
  public void clear(Contest contest);
}
