package org.teamseas.minecraft.contest;

import com.google.common.collect.ImmutableList;
import java.util.List;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

/**
 * Command TAB completer for the "actionbar" command. 
 */
public class WinnerCommandTabCompleter implements TabCompleter {

  /**
   * Requests a list of possible completions for the "winner" command.
   */
  @Override
  public List<String> onTabComplete(CommandSender sender, Command command, String alias,
      String[] args) {
    if (args.length == 1) {
      return ImmutableList.of("list", "add", "remove");
    }
    if (args.length == 2) {
      return ImmutableList.of("daily", "weekly", "monthly");
    }
    return null;
  }

}
