package org.teamseas.minecraft.contest;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import org.bukkit.OfflinePlayer;
import org.teamseas.minecraft.Plugin;
import org.teamseas.minecraft.score.ScoreRepository;

/**
 * Schedule for the end of contests.
 */
public class ContestScheduler {

  private final Plugin plugin;
  private final ScoreRepository scoreRepository;
  private final WinnerRepository winnerRepository;
  private final ContestConfig contestConfig;

  /**
   * Create a contest scheduler instance. 
   */
  public ContestScheduler(Plugin plugin, ScoreRepository scoreRepository,
      WinnerRepository winnerRepository, ContestConfig contestConfig) {
    this.plugin = plugin;
    this.scoreRepository = scoreRepository;
    this.winnerRepository = winnerRepository;
    this.contestConfig = contestConfig;
  }

  /**
   * Starts the daily, weekly and monthly contests.
   */
  public void start() {
    create(Contest.DAILY);
    create(Contest.WEEKLY);
    create(Contest.MONTHLY);
  }

  private void create(Contest contest) {
    long delay = ChronoUnit.SECONDS.between(LocalDateTime.now(), contest.end());
    Runnable task = () -> finished(contest);
    plugin.getServer().getScheduler().runTaskLater(plugin, task, delay * 20);
  }

  private void finished(Contest contest) {
    List<OfflinePlayer> topPlayers = scoreRepository.topPlayers(contest, 10);
    int rank = 1;
    for (OfflinePlayer player : topPlayers) {
      Optional<String> prize = contestConfig.getPrize(contest, rank++, topPlayers.size());
      if (prize.isPresent()) {
        Winner winner = Winner.builder().player(player).prize(prize.get()).build();
        winnerRepository.addWinner(contest, winner);
      }
    }
    scoreRepository.clear(contest);
    create(contest);
  }
}
