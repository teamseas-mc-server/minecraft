package org.teamseas.minecraft.contest;

import lombok.Builder;
import lombok.Getter;
import org.bukkit.OfflinePlayer;

/**
 * A winning event. 
 */
@Builder
@Getter
public class Winner {
  private final OfflinePlayer player;
  private final String prize;
}
