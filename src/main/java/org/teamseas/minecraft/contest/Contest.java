package org.teamseas.minecraft.contest;

import com.google.common.annotations.VisibleForTesting;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import org.bukkit.ChatColor;

/**
 * Different time periods for contests.
 */
public enum Contest {
  DAILY(new DailyAdjuster()),
  WEEKLY(new WeeklyAdjuster()),
  MONTHLY(new MonthlyAdjuster()),
  ALLTIME(new AlltimeAdjuster());
  
  private final TemporalAdjuster adjuster;

  private Contest(TemporalAdjuster adjuster) {
    this.adjuster = adjuster;
  }
  
  public LocalDateTime end() {
    return end(LocalDateTime.now());
  }

  @VisibleForTesting
  LocalDateTime end(LocalDateTime now) {
    return now.with(adjuster);
  }
  
  private static class DailyAdjuster implements TemporalAdjuster {
    @Override
    public Temporal adjustInto(Temporal temporal) {
      return LocalDate.from(temporal).plusDays(1).atStartOfDay();
    }
  }
  
  private static class WeeklyAdjuster implements TemporalAdjuster {
    @Override
    public Temporal adjustInto(Temporal temporal) {
      return LocalDate.from(temporal).with(TemporalAdjusters.next(DayOfWeek.MONDAY)).atStartOfDay();
    }
  }
  
  private static class MonthlyAdjuster implements TemporalAdjuster {
    @Override
    public Temporal adjustInto(Temporal temporal) {
      return LocalDate.from(temporal).with(TemporalAdjusters.firstDayOfNextMonth()).atStartOfDay();
    }
  }
  
  private static class AlltimeAdjuster implements TemporalAdjuster {
    @Override
    public Temporal adjustInto(Temporal temporal) {
      return LocalDateTime.MAX;
    }
  }

  /**
   * Returns a symbol representation of the contest.
   */
  public String getSymbol() {
    switch (this) {
      case DAILY:
        return "○ ";
      case MONTHLY:
        return "□ ";
      case WEEKLY:
        return "△ ";
      case ALLTIME:
      default:
        return "";
    }
  }

  /**
   * Returns color representation of the contest.
   */
  public ChatColor getColor() {
    switch (this) {
      case DAILY:
        return ChatColor.GREEN;
      case WEEKLY:
        return ChatColor.YELLOW;
      case MONTHLY:
        return ChatColor.RED;
      case ALLTIME:
        return ChatColor.DARK_RED;
      default:
        return ChatColor.WHITE;
    }
  }

  /**
   * Converts a string to a contest.
   */
  public static Contest fromString(String name) {
    switch (name) {
      case "daily":
        return DAILY;
      case "weekly":
        return WEEKLY;
      case "monthly":
        return MONTHLY;
      case "alltime":
        return ALLTIME;
      default:
        return null;
    }
  }
}
