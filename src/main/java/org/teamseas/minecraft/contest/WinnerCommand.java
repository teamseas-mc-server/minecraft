package org.teamseas.minecraft.contest;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.teamseas.minecraft.paypal.PaypalRepository;

/**
 * Command to list or modify contest winners.  
 */
public class WinnerCommand implements CommandExecutor {

  private final WinnerRepository winnerRepository;
  private final PaypalRepository paypalRepository;

  public WinnerCommand(WinnerRepository winnerRepository, PaypalRepository paypalRepository) {
    this.winnerRepository = winnerRepository;
    this.paypalRepository = paypalRepository;
  }

  /**
   * Executes the /winner command. 
   */
  @Override
  public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    if (args.length > 1) {
      switch (args[0]) {
        case "list":
          listWinner(sender, args);
          break;
        case "add":
          addWinner(sender, args);
          break;
        case "remove":
          removeWinner(sender, args);
          break;
        default:
          return false;
      }
    }
    return true;
  }

  private void listWinner(CommandSender sender, String[] args) {
    if (args.length == 2) {
      winnerRepository.listWinners(contest(args[1])).forEach(winner -> {
        OfflinePlayer player = winner.getPlayer();
        String email = paypalRepository.getPaypalEmail(player).orElse("");
        sender.sendMessage(player.getName() + " " + winner.getPrize() + " " + email);
      });
    }
  }

  private void addWinner(CommandSender sender, String[] args) {
    if (args.length == 4) {
      Winner winner = Winner.builder().player(player(args[2])).prize(args[3]).build();
      winnerRepository.addWinner(contest(args[1]), winner);
    }
  }

  private void removeWinner(CommandSender sender, String[] args) {
    if (args.length == 3) {
      OfflinePlayer player = player(args[2]);
      winnerRepository.removeWinner(contest(args[1]), winner -> winner.getPlayer().equals(player));
    }
  }

  private static Contest contest(String contextName) {
    return Contest.fromString(contextName);
  }

  private static OfflinePlayer player(String playerName) {
    return Bukkit.getOfflinePlayer(playerName);
  }

}
