package org.teamseas.minecraft.contest;

import java.util.List;
import java.util.function.Predicate;

/**
 * This repository stores contest winners. 
 */
public interface WinnerRepository {

  void addWinner(Contest contest, Winner winner);

  void removeWinner(Contest contest, Predicate<Winner> predicate);
  
  List<Winner> listWinners(Contest contest);
  
}
