package org.teamseas.minecraft.contest;

import com.google.common.collect.ImmutableList;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * Configuration about contests and their prizes. 
 */
public class DefaultContestConfig implements ContestConfig {

  @Override
  public Optional<String> getPrize(Contest contest, int rank, int participants) {
    final List<String> prizes;
    switch (contest) {
      case WEEKLY:
        prizes = ImmutableList.of("$3", "$2", "$1");
        break;
      case MONTHLY:
        prizes = ImmutableList.of("$5", "$3", "$2", "$1");
        break;
      case DAILY:
      case ALLTIME:
      default:
        prizes = ImmutableList.of();
        break;
    }
    int reduce = participants < prizes.size() ? prizes.size() - participants : 0;
    return ofThrowable(() -> prizes.get(rank - 1 + reduce));
  }

  private static <T> Optional<T> ofThrowable(Supplier<T> supplier) {
    try {
      return Optional.of(supplier.get());
    } catch (Throwable exception) {
      return Optional.empty();
    }
  }

}
