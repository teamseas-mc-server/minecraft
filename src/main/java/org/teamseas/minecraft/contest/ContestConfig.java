package org.teamseas.minecraft.contest;

import java.util.Optional;

/**
 * Configuration about contests and their prizes. 
 */
public interface ContestConfig {

  Optional<String> getPrize(Contest contest, int rank, int participants);

}
