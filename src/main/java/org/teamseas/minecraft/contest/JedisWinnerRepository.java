package org.teamseas.minecraft.contest;

import java.util.List;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import redis.clients.jedis.Jedis;

/**
 * Redis implementation of winner repository.
 */
public class JedisWinnerRepository implements WinnerRepository {
  private final Jedis jedis;

  public JedisWinnerRepository(Jedis jedis) {
    this.jedis = jedis;
  }

  @Override
  public void addWinner(Contest contest, Winner winner) {
    jedis.rpush(key(contest), serialize(winner));
  }

  @Override
  public void removeWinner(Contest contest, Predicate<Winner> predicate) {
    long len = jedis.llen(key(contest));
    for (long index = 0; index < len; index++) {
      Winner winner = deserialize(jedis.lindex(key(contest), index));
      if (predicate.test(winner)) {
        jedis.lset(key(contest), index, "DELETED");
      }
    }
    jedis.lrem(key(contest), 0, "DELETED");
  }

  @Override
  public List<Winner> listWinners(Contest contest) {
    return jedis.lrange(key(contest), 0, -1).stream()
        .map(JedisWinnerRepository::deserialize)
        .collect(Collectors.toList());
  }

  private static String key(Contest contest) {
    return contest.name() + ":WINNER";
  }

  private static String serialize(Winner winner) {
    String player = winner.getPlayer().getUniqueId().toString();
    return player + ":" + winner.getPrize();
  }
  
  private static Winner deserialize(String serialized) {
    String[] parts = serialized.split(":");
    UUID uuid = UUID.fromString(parts[0]);
    OfflinePlayer player = Bukkit.getOfflinePlayer(uuid);
    String prize = parts[1];
    return Winner.builder().player(player).prize(prize).build();
  }

}
