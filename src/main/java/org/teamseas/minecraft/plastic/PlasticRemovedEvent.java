package org.teamseas.minecraft.plastic;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Event that is fired when a player changes plastic count in the world (increment, or decrement).
 */
@Getter
@AllArgsConstructor
public class PlasticRemovedEvent extends Event {
  private static final HandlerList handlers = new HandlerList();

  private Player player;
  private int count;

  @Override
  public HandlerList getHandlers() {
    return handlers;
  }

  public static HandlerList getHandlerList() {
    return handlers;
  }
}
