package org.teamseas.minecraft.plastic;

import java.util.NavigableMap;
import java.util.Random;
import java.util.TreeMap;

/**
 * A collection to pick a random element from with weighed probability.
 *
 * @link https://stackoverflow.com/a/6409791
 */
public class RandomCollection<E> {
  private final NavigableMap<Double, E> map = new TreeMap<Double, E>();
  private final Random random;
  private double total = 0;

  public RandomCollection() {
    this(new Random());
  }

  public RandomCollection(Random random) {
    this.random = random;
  }

  /**
   * Adds given element with the given weight to the collection.
   */
  public void add(double weight, E result) {
    if (weight <= 0) {
      return;
    }

    total += weight;
    map.put(total, result);
  }

  public E next() {
    double value = random.nextDouble() * total;
    return map.higherEntry(value).getValue();
  }
} 
