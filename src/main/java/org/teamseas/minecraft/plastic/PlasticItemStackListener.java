package org.teamseas.minecraft.plastic;

import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.ItemDespawnEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

/**
 * Dispatch several events for PlasticItemStack.
 */
public class PlasticItemStackListener implements Listener {

  private static final Random random = new Random();

  /**
   * Player picks an plastic item up from the sea. This gives the player one
   * score point and one experience orb. This plastic item is marked as
   * "unsigned", so that we can see later that it has already been counted.
   */
  @EventHandler
  public void onPlayerPickupItemEvent(EntityPickupItemEvent event) {
    ItemStack itemStack = event.getItem().getItemStack();
    if (!PlasticItemStack.isSigned(itemStack)) {
      return;
    }
    LivingEntity entity = event.getEntity();
    if (entity instanceof Player) {
      Player player = (Player) entity;
      plasticRemovedEvent(player, itemStack.getAmount());
      dropExperience(player, itemStack.getAmount());
      PlasticItemStack.unsign(itemStack);
    }
  }

  private static void plasticRemovedEvent(Player player, int amount) {
    Bukkit.getPluginManager().callEvent(new PlasticRemovedEvent(player, amount));
  }

  private static void dropExperience(Player player, int amount) {
    ExperienceOrb orb = (ExperienceOrb) player.getWorld()
        .spawnEntity(player.getLocation().add(randomDistance()), EntityType.EXPERIENCE_ORB);
    if (orb != null) {
      orb.setExperience(amount);
    }
  }

  private static Vector randomDistance() {
    double x = random.nextDouble() * 2.0 - 1.0;
    double y = random.nextDouble() * 2.0;
    double z = random.nextDouble() * 2.0 - 1.0;
    return new Vector(x, y, z);
  }

  /**
   * Player drops plastic items from their inventory. This cost some experience
   * (if there is any left), because we want to keep the environment clean.
   */
  @EventHandler(priority = EventPriority.LOWEST)
  public void onPlayerDropItemEvent(PlayerDropItemEvent event) {
    ItemStack itemStack = event.getItemDrop().getItemStack();
    if (!PlasticItemStack.is(itemStack)) {
      return;
    }
    Player player = event.getPlayer();
    int amount = Math.min(itemStack.getAmount(), player.getTotalExperience());
    player.giveExp(-amount);
  }

  /**
   * Item is not removed from the world after a certain time.
   */
  @EventHandler(priority = EventPriority.LOWEST)
  public void onItemDespawnEvent(ItemDespawnEvent event) {
    ItemStack itemStack = event.getEntity().getItemStack();
    if (PlasticItemStack.isSigned(itemStack)) {
      event.setCancelled(true);
    }
  }

}
