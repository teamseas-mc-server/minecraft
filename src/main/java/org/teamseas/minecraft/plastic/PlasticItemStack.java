package org.teamseas.minecraft.plastic;

import com.google.common.collect.ImmutableList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Item of plastic waste.
 */
public class PlasticItemStack extends ItemStack {

  private static final String IDENT = "Removed from the ocean by teamseas.org";
  private static final String SIGNATURE = "effective";

  @RequiredArgsConstructor
  private static enum Materials {
    PLASTIC_BOTTLE(Material.GLASS_BOTTLE, "Plastic bottle", 5),
    PLASTIC_BUCKET(Material.BUCKET, "Plastic bucket", 1),
    PLASTIC_BOTTLE_CAP(Material.BOWL, "Plastic bottle cap", 5),
    PLASTIC_STRAW(Material.STICK, "Plastic straw", 1),
    PLASTIC_CONTAINER(Material.MINECART, "Plastic container", 1),
    MASK(Material.PAPER, "Mask", 5),
    PLASTIC_WRAPPER(Material.STRING, "Plastic Wrapper", 5);

    private final Material material;
    private final String display;
    private final double weight;
  }

  private static final List<Materials> MATERIALS = ImmutableList.copyOf(Materials.values());
  private static final RandomCollection<Materials> RANDOM_MATERIALS = 
      getRandomCollection(MATERIALS);

  private static RandomCollection<Materials> getRandomCollection(List<Materials> materials) {
    RandomCollection<Materials> collection = new RandomCollection<>();
    materials.forEach(material -> collection.add(material.weight, material));
    return collection;
  }

  public PlasticItemStack() {
    super(createMaterial(), 1);
    updateItemMeta();
  }

  private static Material createMaterial() {
    return RANDOM_MATERIALS.next().material;
  }

  private void updateItemMeta() {
    ItemMeta itemMeta = getItemMeta();
    itemMeta.setDisplayName(findByMaterial(getType()).display);
    itemMeta.setLore(createLore());
    setItemMeta(itemMeta);
  }

  private static Materials findByMaterial(Material material) {
    return MATERIALS.stream().filter(e -> e.material.equals(material)).findFirst().get();
  }

  private static List<String> createLore() {
    return ImmutableList.of(IDENT, SIGNATURE);
  }

  /**
   * An initial plastic item stack has a signature.
   */
  public static boolean isSigned(ItemStack itemStack) {
    if (is(itemStack)) {
      List<String> lore = itemStack.getLore();
      if (lore != null && lore.size() > 1) {
        return lore.get(1).equals(SIGNATURE);
      }
    }
    return false;
  }

  /**
   * Mark a plastic item stack as no longer new.
   */
  public static void unsign(ItemStack itemStack) {
    if (is(itemStack)) {
      List<String> lore = itemStack.getLore();
      if (lore != null && lore.size() > 1) {
        lore.remove(1);
      }
      itemStack.setLore(lore);
    }
  }

  /**
   * Is the given itemStack a PlasticItemStack.
   */
  public static boolean is(ItemStack itemStack) {
    Material material = itemStack.getType();
    if (!materials().contains(material)) {
      return false;
    }
    ItemMeta itemMeta = itemStack.getItemMeta();
    if (!itemMeta.hasLore()) {
      return false;
    }
    List<String> lore = itemMeta.getLore();
    if ((lore == null) || (lore.size() < 1)) {
      return false;
    }
    return lore.get(0).equals(IDENT);
  }

  private static Set<Material> materials() {
    return MATERIALS.stream().map(e -> e.material).collect(Collectors.toSet());
  }
}
