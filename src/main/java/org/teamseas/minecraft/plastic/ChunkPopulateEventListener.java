package org.teamseas.minecraft.plastic;

import com.google.common.collect.ImmutableSet;
import de.ixilon.bukkit.plugin.terrain.TerrainPlugin;
import java.util.Random;
import java.util.Set;
import java.util.function.Consumer;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Biome;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkPopulateEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.scheduler.BukkitScheduler;
import org.teamseas.minecraft.Plugin;

/**
 * Creates floating plastic waste in the ocean.
 */
public class ChunkPopulateEventListener implements Listener {

  private static final float METERS_PER_SQUARE_KILOMETER = 1000F * 1000F;

  private static Set<Biome> BIOMES = ImmutableSet.of(Biome.BEACH, Biome.COLD_OCEAN,
      Biome.DEEP_COLD_OCEAN, Biome.DEEP_LUKEWARM_OCEAN, Biome.DEEP_OCEAN, Biome.DEEP_WARM_OCEAN,
      Biome.LUKEWARM_OCEAN, Biome.OCEAN, Biome.SNOWY_BEACH, Biome.WARM_OCEAN);

  private final Random random = new Random();
  private final Plugin plugin;

  public ChunkPopulateEventListener(Plugin plugin) {
    this.plugin = plugin;
  }

  /**
   * Called when a new chunk has finished being populated.
   */
  @EventHandler(priority = EventPriority.LOWEST)
  public void onChunkPopulateEvent(ChunkPopulateEvent event) {
    scheduler().runTaskAsynchronously(plugin, () -> processChunkPopulateEvent(event.getChunk()));
  }

  private BukkitScheduler scheduler() {
    return plugin.getServer().getScheduler();
  }

  private void processChunkPopulateEvent(Chunk chunk) {
    if (!isRelevantBiome(chunk)) {
      return;
    }
    foreach(chunk, location -> {
      if (isPolluted(location)) {
        scheduler().runTask(plugin, () -> dropItem(location));
      }
    });
  }

  private static boolean isRelevantBiome(Chunk chunk) {
    for (int x = 0; x < 16; x += 4) {
      for (int z = 0; z < 16; z += 4) {
        if (!BIOMES.contains(chunk.getBlock(x, 0, z).getBiome())) {
          return false;
        }
      }
    }
    return true;
  }

  private static void foreach(Chunk chunk, Consumer<Location> consumer) {
    World world = chunk.getWorld();
    int xoff = chunk.getX() << 4;
    int zoff = chunk.getZ() << 4;
    for (int dx = 0; dx < 16; dx++) {
      for (int dz = 0; dz < 16; dz++) {
        int x = xoff + dx;
        int z = zoff + dz;
        int y = world.getHighestBlockYAt(x, z);
        Location location = new Location(world, x, y, z).add(0.5, 1.0, 0.5);
        consumer.accept(location);
      }
    }
  }

  private boolean isPolluted(Location location) {
    PluginManager pluginManager = Bukkit.getServer().getPluginManager();
    String pluginName = "openstreetcraft-terrain";
    TerrainPlugin terrain = (TerrainPlugin) pluginManager.getPlugin(pluginName);
    if (terrain != null) {
      try {
        float pollution =
            terrain.plastic(location.getWorld(), location) / METERS_PER_SQUARE_KILOMETER;
        return random.nextFloat() < pollution;
      } catch (IllegalArgumentException exception) {
        // wrong world? use default
      }
    }
    return random.nextInt(10) == 0;
  }

  private void dropItem(Location location) {
    location.getWorld().dropItem(location, new PlasticItemStack());
  }

}
