package org.teamseas.minecraft.actionbar;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Actionbar command which changes visibility settings.
 */
public class ActionbarCommand implements CommandExecutor {

  private final ActionbarRepository repository;
  private final Actionbar actionbar;

  public ActionbarCommand(ActionbarRepository repository, Actionbar actionbar) {
    this.repository = repository;
    this.actionbar = actionbar;
  }

  @Override
  public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    Player player = (Player) sender;

    if (args.length == 0) {
      if (repository.isVisible(player)) {
        hide(player);
      } else {
        show(player);
      }

      return true;
    }

    switch (args[0]) {
      case "on":
      case "show":
        show(player);
        break;
      case "off":
      case "hide":
        hide(player);
        break;
      default:
        break;
    }

    return true;
  }

  private void show(Player player) {
    actionbar.setVisible(player);
    repository.setVisible(player, true);
    sendVisibilityMessage(player, "visible");
  }

  private void hide(Player player) {
    actionbar.setInvisible(player);
    repository.setVisible(player, false);
    sendVisibilityMessage(player, "invisible");
  }


  private static void sendVisibilityMessage(Player player, String visibility) {
    player.sendMessage(color("&aActionbar is now " + visibility + "."));
  }

  private static String color(String text) {
    return ChatColor.translateAlternateColorCodes('&', text);
  }
}
