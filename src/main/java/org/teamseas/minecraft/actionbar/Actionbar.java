package org.teamseas.minecraft.actionbar;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Displays the contest statistics on bottom of screen.
 */
public class Actionbar {

  private final Map<Player, String> bars = new ConcurrentHashMap<>();

  private final ActionbarRepository repository;

  private final Listener listener;

  private final Runnable updaterTask;
  private final Runnable displayTask;

  /**
   * Constructor for Actionbar.
   */
  public Actionbar(ActionbarRepository repository, ActionbarService service) {
    this.repository = repository;

    listener = new ActionbarListener();

    updaterTask = new ActionbarUpdaterTask(service);
    displayTask = new ActionbarDisplayTask();
  }

  public void setVisible(Player player) {
    bars.put(player, "");
  }

  public void setInvisible(Player player) {
    bars.remove(player);
  }

  public Listener getListener() {
    return listener;
  }

  public Runnable getUpdaterTask() {
    return updaterTask;
  }

  public Runnable getDisplayTask() {
    return displayTask;
  }

  private class ActionbarListener implements Listener {
    /**
     * Adds player to the actionbar map when player joins.
     */
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
      Player player = event.getPlayer();

      if (!player.hasPlayedBefore()) {
        repository.setVisible(player, true);
      }

      if (repository.isVisible(player)) {
        setVisible(player);
      }
    }

    /**
     * Removes player from actionbar map when player quits.
     */
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
      Player player = event.getPlayer();
      setInvisible(player);
    }
  }

  private class ActionbarUpdaterTask implements Runnable {
    private ActionbarService service;

    public ActionbarUpdaterTask(ActionbarService service) {
      this.service = service;
    }

    @Override
    public void run() {
      bars.replaceAll((player, message) -> service.getMessage(player));
    }
  }

  private class ActionbarDisplayTask implements Runnable {
    @Override
    public void run() {
      bars.forEach((player, message) -> player.sendActionBar(message));
    }
  }
}
