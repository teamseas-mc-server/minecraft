package org.teamseas.minecraft.actionbar;

import org.bukkit.OfflinePlayer;

/**
 * Data repository to store the player specific configuration of it's actionbar.
 */
public interface ActionbarRepository {
  
  void setVisible(OfflinePlayer player, boolean isVisible);

  boolean isVisible(OfflinePlayer player);
}
