package org.teamseas.minecraft.actionbar;

import com.google.common.collect.ImmutableList;
import java.util.List;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

/**
 * Command TAB completer for the "actionbar" command. 
 */
public class ActionbarCommandTabCompleter implements TabCompleter {

  /**
   * Requests a list of possible completions for the "actionbar" command.
   */
  @Override
  public List<String> onTabComplete(CommandSender sender, Command command, String alias,
      String[] args) {
    if (args.length == 1) {
      return ImmutableList.of("on", "off", "show", "hide");
    }
    return null;
  }

}
