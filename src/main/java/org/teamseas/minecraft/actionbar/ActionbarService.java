package org.teamseas.minecraft.actionbar;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import java.util.Locale;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.teamseas.minecraft.contest.Contest;
import org.teamseas.minecraft.score.ScoreRepository;
import org.teamseas.minecraft.util.IntegerPrinter;

/**
 * Helper class to generate new text for actionbar.
 */
public class ActionbarService {

  private final IntegerPrinter printer = new IntegerPrinter();
  private final ScoreRepository repository;

  public ActionbarService(ScoreRepository repository) {
    this.repository = repository;
  }

  /**
   * Grabs new player statistics and returns message.
   */
  public String getMessage(Player player) {
    ImmutableList.Builder<String> messages = ImmutableList.builder();
    ImmutableList.copyOf(Contest.values()).forEach(contest -> {
      String score = printer.print(repository.getScore(contest, player), Locale.US);
      int rank = repository.getRank(contest, player);
      String text = String.format("%s (%s#%d)", score, contest.getSymbol(), rank);
      messages.add(contest.getColor() + text);
    });

    return Joiner.on(ChatColor.GRAY + " | ").join(messages.build());
  }
}
