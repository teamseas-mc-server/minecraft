package org.teamseas.minecraft.actionbar;

import org.bukkit.OfflinePlayer;
import redis.clients.jedis.Jedis;

/**
 * Redis implementation of actionbar configuration repository.
 */
public class JedisActionbarRepository implements ActionbarRepository {

  private final Jedis jedis;
  private static final String ACTIONBAR_KEY = "ACTIONBAR:VISIBLE";

  public JedisActionbarRepository(Jedis jedis) {
    this.jedis = jedis;
  }

  @Override
  public void setVisible(OfflinePlayer player, boolean isVisible) {
    jedis.hset(ACTIONBAR_KEY, hashKey(player), Boolean.toString(isVisible));
  }

  @Override
  public boolean isVisible(OfflinePlayer player) {
    return Boolean.parseBoolean(jedis.hget(ACTIONBAR_KEY, hashKey(player)));
  }

  private static String hashKey(OfflinePlayer player) {
    return player.getUniqueId().toString();
  }
}
