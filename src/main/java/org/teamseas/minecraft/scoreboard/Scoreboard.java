package org.teamseas.minecraft.scoreboard;

import fr.mrmicky.fastboard.FastBoard;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Displays the highscore permanently on the screen.
 *
 * @link https://github.com/MrMicky-FR/FastBoard
 */
public class Scoreboard {

  private final Map<UUID, FastBoard> boards = new ConcurrentHashMap<>();

  private final ScoreboardRepository repository;
  private final ScoreboardService service;
  private final Listener listener;
  private final Runnable timerTask;

  /**
   * Create a scoreboard. 
   */
  public Scoreboard(ScoreboardRepository repository, ScoreboardService service) {
    this.repository = repository;
    this.service = service;
    listener = new ScoreboardListener();
    timerTask = new ScoreboardTimerTask();
  }
  
  /**
   * Show the player his scoreboard. 
   */
  public void setVisible(Player player) {
    boards.computeIfAbsent(player.getUniqueId(), uuid -> {
      FastBoard board = new FastBoard(player);
      board.updateTitle(color("&bTeam Seas"));
      return board;
    });
  }

  /**
   * Hide the scoreboard from the player. 
   */
  public void setInvisible(Player player) {
    FastBoard board = boards.remove(player.getUniqueId());
    if (board != null) {
      board.delete();
    }
  }

  private static String color(String string) {
    return ChatColor.translateAlternateColorCodes('&', string);
  }

  public Listener getListener() {
    return listener;
  }

  public Runnable getTimerTask() {
    return timerTask;
  }

  private class ScoreboardListener implements Listener {
    /**
     * Adds player to the scoreboard map when player joins.
     */
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
      Player player = event.getPlayer();
      if (repository.isVisible(player)) {
        setVisible(player);
      }
    }

    /**
     * Removes player from scoreboard map when player quits.
     */
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
      Player player = event.getPlayer();
      setInvisible(player);
    }
  }

  private class ScoreboardTimerTask implements Runnable {
    @Override
    public void run() {
      boards.forEach((uuid, board) -> board.updateLines(service.getLines(player(uuid))));
    }

    private OfflinePlayer player(UUID uuid) {
      return Bukkit.getOfflinePlayer(uuid);
    }
  }
}
