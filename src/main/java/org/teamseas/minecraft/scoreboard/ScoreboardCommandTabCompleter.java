package org.teamseas.minecraft.scoreboard;

import com.google.common.collect.ImmutableList;
import java.util.List;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

/**
 * Command TAB completer for the "scoreboard" command. 
 */
public class ScoreboardCommandTabCompleter implements TabCompleter {

  /**
   * Requests a list of possible completions for the "scoreboard" command.
   */
  @Override
  public List<String> onTabComplete(CommandSender sender, Command command, String alias,
      String[] args) {
    if (args.length == 1) {
      return ImmutableList.of("on", "off", "show", "hide", "daily", "weekly", "monthly", "alltime");
    }
    return null;
  }

}
