package org.teamseas.minecraft.scoreboard;

import com.google.common.base.Joiner;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.teamseas.minecraft.contest.Contest;

/**
 * Scoreboard command which displays total count and top players.
 */
public class ScoreboardCommand implements CommandExecutor {

  private final ScoreboardRepository repository;
  private final ScoreboardService service;
  private final Scoreboard scoreboard;

  /**
   * Creates the "scoreboard" command executor. 
   */
  public ScoreboardCommand(ScoreboardRepository repository, ScoreboardService service,
      Scoreboard scoreboard) {
    this.repository = repository;
    this.service = service;
    this.scoreboard = scoreboard;
  }

  /**
   * Called on /scoreboard command.
   */
  @Override
  public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    Player player = (Player) sender;

    if (args.length == 0) {
      sendMessage(player);
      return true;
    }

    switch (args[0]) {
      case "on":
      case "show":
        scoreboard.setVisible(player);
        repository.setVisible(player, true);
        sendVisibilityMessage(player, "visible");
        break;
      case "off":
      case "hide":
        scoreboard.setInvisible(player);
        repository.setVisible(player, false);
        sendVisibilityMessage(player, "invisible");
        break;
      case "daily":
        repository.setContest(player, Contest.DAILY);
        sendContestMessage(player, Contest.DAILY);
        break;
      case "weekly":
        repository.setContest(player, Contest.WEEKLY);
        sendContestMessage(player, Contest.WEEKLY);
        break;
      case "monthly":
        repository.setContest(player, Contest.MONTHLY);
        sendContestMessage(player, Contest.MONTHLY);
        break;
      case "alltime":
        repository.setContest(player, Contest.ALLTIME);
        sendContestMessage(player, Contest.ALLTIME);
        break;
      default:
        break;
    }

    if (!repository.isVisible(player)) {
      switch (args[0]) {
        case "daily":
        case "weekly":
        case "monthly":
        case "alltime":
          sendMessage(player);
          break;
        default:
          break;
      }
    }

    return true;
  }

  private void sendMessage(Player player) {
    player.sendMessage(Joiner.on("\n").join(service.getLines(player)));
  }

  private static void sendVisibilityMessage(Player player, String visibility) {
    player.sendMessage(color("&aScoreboard is now " + visibility + "."));
  }

  private static void sendContestMessage(Player player, Contest contest) {
    player.sendMessage(color("&aScoreboard is displaying the " + contest.name() + " contest."));
  }

  private static String color(String text) {
    return ChatColor.translateAlternateColorCodes('&', text);
  }
}
