package org.teamseas.minecraft.scoreboard;

import java.util.Optional;
import org.bukkit.OfflinePlayer;
import org.teamseas.minecraft.contest.Contest;
import redis.clients.jedis.Jedis;

/**
 * Redis implementation of scoreboard configuration repository.
 */
public class JedisScoreboardRepository implements ScoreboardRepository {

  private final Jedis jedis;

  public JedisScoreboardRepository(Jedis jedis) {
    this.jedis = jedis;
  }

  @Override
  public void setVisible(OfflinePlayer player, boolean isVisible) {
    jedis.hset(key("VISIBLE"), hashKey(player), Boolean.toString(isVisible));
  }

  @Override
  public boolean isVisible(OfflinePlayer player) {
    return Boolean.parseBoolean(jedis.hget(key("VISIBLE"), hashKey(player)));
  }

  @Override
  public void setContest(OfflinePlayer player, Contest contest) {
    jedis.hset(key("CONTEST"), hashKey(player), contest.name());
  }

  @Override
  public Optional<Contest> getContest(OfflinePlayer player) {
    return Optional.ofNullable(jedis.hget(key("CONTEST"), hashKey(player))).map(Contest::valueOf);
  }

  private static String key(String category) {
    return "SCOREBOARD:" + category;
  }

  private static String hashKey(OfflinePlayer player) {
    return player.getUniqueId().toString();
  }

}
