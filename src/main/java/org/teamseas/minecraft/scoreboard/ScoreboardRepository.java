package org.teamseas.minecraft.scoreboard;

import java.util.Optional;
import org.bukkit.OfflinePlayer;
import org.teamseas.minecraft.contest.Contest;

/**
 * Data repository to store the player specific configuration of its scoreboard.
 */
public interface ScoreboardRepository {
  
  void setVisible(OfflinePlayer player, boolean isVisible);

  boolean isVisible(OfflinePlayer player);
  
  void setContest(OfflinePlayer player, Contest contest);
  
  Optional<Contest> getContest(OfflinePlayer player);
}
