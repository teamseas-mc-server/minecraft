package org.teamseas.minecraft.scoreboard;

import com.google.common.collect.ImmutableList;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.teamseas.minecraft.contest.Contest;
import org.teamseas.minecraft.contest.ContestConfig;
import org.teamseas.minecraft.score.ScoreRepository;
import org.teamseas.minecraft.util.IntegerPrinter;

/**
 * Helper class to generate new text for scoreboard.
 */
public class ScoreboardService {

  private final IntegerPrinter printer = new IntegerPrinter();
  private final ScoreboardRepository scoreboardRepository;
  private final ScoreRepository scoreRepository;
  private final ContestConfig contestConfig;

  /**
   * Creates a scoreboard service instance. 
   */
  public ScoreboardService(ScoreboardRepository scoreboardRepository,
      ScoreRepository scoreRepository, ContestConfig contestConfig) {
    this.scoreboardRepository = scoreboardRepository;
    this.scoreRepository = scoreRepository;
    this.contestConfig = contestConfig;
  }

  /**
   * Grabs new statistics for scoreboard and returns new lines.
   */
  public ImmutableList<String> getLines(OfflinePlayer currentPlayer) {
    ImmutableList.Builder<String> lines = ImmutableList.builder();
    Contest contest = scoreboardRepository.getContest(currentPlayer).orElse(Contest.ALLTIME);
        
    lines.add("&dLet's save our oceans");
    
    switch (contest) {
      case DAILY:
        lines.add(String.format("&a○ Daily contest %s left", left(contest)));
        break;
      case WEEKLY:
        lines.add(String.format("&a△ Weekly contest %s left", left(contest)));
        break;
      case MONTHLY:
        lines.add(String.format("&a□ Monthly contest %s left", left(contest)));
        break;
      default:
        break;
    }
    
    lines.add("");
    lines.add("&aTop plastic removers:");

    int rank = 1;
    int numPlayers = 10;
    List<OfflinePlayer> topPlayers = scoreRepository.topPlayers(contest, numPlayers);
    int participants = topPlayers.size();
    
    for (OfflinePlayer player : topPlayers) {
      Optional<String> prize = contestConfig.getPrize(contest, rank++, participants);
      addPlayer(lines, player, contest, prize);
    }
    
    int currentRank = scoreRepository.getRank(contest, currentPlayer);
    
    if (currentRank > numPlayers) {
      Optional<String> prize = contestConfig.getPrize(contest, currentRank, participants);
      addPlayer(lines, currentPlayer, contest, prize);
    }

    int total = scoreRepository.getTotalScore(contest);

    lines.add(String.format("&aTotal plastic removed: &6%s", printer.print(total, Locale.US)));
    lines.add("", "&dteamseas.org");

    return color(lines.build());

  }

  private static String left(Contest contest) {
    long left = LocalDateTime.now().until(contest.end(), ChronoUnit.MINUTES);
    return String.format("%02d:%02d", left / 60, left % 60);
  }

  private void addPlayer(ImmutableList.Builder<String> lines, OfflinePlayer player,
      Contest contest, Optional<String> prize) {
    int rank = scoreRepository.getRank(contest, player);
    String playerName = player.getName();
    if (playerName != null) {
      int score = scoreRepository.getScore(contest, player);
      String formattedScore = printer.print(score, Locale.US);
      String line = String.format("&6#%d &b%s &b(&6%s&b)", rank, playerName, formattedScore);
      if (prize.isPresent()) {
        line += String.format(" - &6%s", prize.get());
      }
      lines.add(line);
    }
  }

  private static ImmutableList<String> color(ImmutableList<String> lines) {
    return lines.stream().map((line) -> color(line)).collect(ImmutableList.toImmutableList());
  }

  private static String color(String string) {
    return ChatColor.translateAlternateColorCodes('&', string);
  }
}
