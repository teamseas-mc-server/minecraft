package org.teamseas.minecraft;

import org.bukkit.command.CommandExecutor;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.TabCompleter;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.teamseas.minecraft.actionbar.Actionbar;
import org.teamseas.minecraft.actionbar.ActionbarCommand;
import org.teamseas.minecraft.actionbar.ActionbarCommandTabCompleter;
import org.teamseas.minecraft.actionbar.ActionbarRepository;
import org.teamseas.minecraft.actionbar.ActionbarService;
import org.teamseas.minecraft.actionbar.JedisActionbarRepository;
import org.teamseas.minecraft.contest.ContestConfig;
import org.teamseas.minecraft.contest.ContestScheduler;
import org.teamseas.minecraft.contest.DefaultContestConfig;
import org.teamseas.minecraft.contest.JedisWinnerRepository;
import org.teamseas.minecraft.contest.WinnerCommand;
import org.teamseas.minecraft.contest.WinnerCommandTabCompleter;
import org.teamseas.minecraft.contest.WinnerRepository;
import org.teamseas.minecraft.paypal.JedisPaypalRepository;
import org.teamseas.minecraft.paypal.PaypalCommand;
import org.teamseas.minecraft.paypal.PaypalListener;
import org.teamseas.minecraft.paypal.PaypalRepository;
import org.teamseas.minecraft.plastic.ChunkPopulateEventListener;
import org.teamseas.minecraft.plastic.PlasticItemStackListener;
import org.teamseas.minecraft.score.JedisScoreRepository;
import org.teamseas.minecraft.score.ScoreCommand;
import org.teamseas.minecraft.score.ScoreCommandTabCompleter;
import org.teamseas.minecraft.score.ScoreRepository;
import org.teamseas.minecraft.score.ScoreRepositoryListener;
import org.teamseas.minecraft.scoreboard.JedisScoreboardRepository;
import org.teamseas.minecraft.scoreboard.Scoreboard;
import org.teamseas.minecraft.scoreboard.ScoreboardCommand;
import org.teamseas.minecraft.scoreboard.ScoreboardCommandTabCompleter;
import org.teamseas.minecraft.scoreboard.ScoreboardRepository;
import org.teamseas.minecraft.scoreboard.ScoreboardService;
import redis.clients.jedis.Jedis;

/**
 * The main class of the plugin.
 */
public class Plugin extends JavaPlugin {

  private Jedis jedis;

  @Override
  public void onEnable() {
    PluginManager pluginManager = getServer().getPluginManager();

    pluginManager.registerEvents(new ChunkPopulateEventListener(this), this);
    pluginManager.registerEvents(new PlasticItemStackListener(), this);

    String host = System.getenv().getOrDefault("REDIS_HOST", "localhost");
    String port = System.getenv().getOrDefault("REDIS_PORT", "6379");
    
    jedis = new Jedis(host, Integer.parseInt(port));

    ScoreRepository scoreRepository = new JedisScoreRepository(jedis);

    pluginManager.registerEvents(new ScoreRepositoryListener(scoreRepository), this);

    // Scoreboard
    ContestConfig contestConfig = new DefaultContestConfig();
    ScoreboardRepository scoreboardRepository = new JedisScoreboardRepository(jedis);
    ScoreboardService scoreboardService =
        new ScoreboardService(scoreboardRepository, scoreRepository, contestConfig);
    Scoreboard scoreboard = new Scoreboard(scoreboardRepository, scoreboardService);
    
    pluginManager.registerEvents(scoreboard.getListener(), this);
    getServer().getScheduler().runTaskTimer(this, scoreboard.getTimerTask(), 0, 3 * 20);
    registerCommand("scoreboard",
        new ScoreboardCommand(scoreboardRepository, scoreboardService, scoreboard));
    registerTabCompleter("scoreboard", new ScoreboardCommandTabCompleter());

    // Actionbar
    ActionbarRepository actionbarRepository = new JedisActionbarRepository(jedis);
    ActionbarService actionbarService = new ActionbarService(scoreRepository);
    Actionbar actionbar = new Actionbar(actionbarRepository, actionbarService);

    pluginManager.registerEvents(actionbar.getListener(), this);
    getServer().getScheduler().runTaskTimer(this, actionbar.getUpdaterTask(), 0, 3 * 20);
    getServer().getScheduler().runTaskTimer(this, actionbar.getDisplayTask(), 0, 10);
    registerCommand("actionbar", new ActionbarCommand(actionbarRepository, actionbar));
    registerTabCompleter("actionbar", new ActionbarCommandTabCompleter());

    // Score command
    registerCommand("score", new ScoreCommand(scoreRepository));
    registerTabCompleter("score", new ScoreCommandTabCompleter());

    // Paypal
    PaypalRepository paypalRepository = new JedisPaypalRepository(jedis);

    registerCommand("paypal", new PaypalCommand(paypalRepository));

    // Contest
    WinnerRepository winnerRepository = new JedisWinnerRepository(jedis);
    ContestScheduler scheduler =
        new ContestScheduler(this, scoreRepository, winnerRepository, contestConfig);

    registerCommand("winner", new WinnerCommand(winnerRepository, paypalRepository));
    registerTabCompleter("winner", new WinnerCommandTabCompleter());

    scheduler.start();
    
    PaypalListener paypalListener = new PaypalListener(this, paypalRepository, winnerRepository);
    
    pluginManager.registerEvents(paypalListener, this);
  }

  private void registerCommand(String commandName, CommandExecutor commandExecutor) {
    PluginCommand command = getCommand(commandName);
    if (command != null) { 
      command.setExecutor(commandExecutor);
    }
  }

  private void registerTabCompleter(String commandName, TabCompleter tabCompleter) {
    PluginCommand command = getCommand(commandName);
    if (command != null) { 
      command.setTabCompleter(tabCompleter);
    }
  }

  @Override
  public void onDisable() {
    if (jedis != null) {
      jedis.close();
      jedis = null;
    }
  }
}
