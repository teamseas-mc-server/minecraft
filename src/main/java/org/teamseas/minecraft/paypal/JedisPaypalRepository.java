package org.teamseas.minecraft.paypal;

import java.util.Optional;
import org.bukkit.OfflinePlayer;
import redis.clients.jedis.Jedis;

/**
 * Redis implementation of paypal repository.
 */
public class JedisPaypalRepository implements PaypalRepository {

  private static final String PAYPAL_KEY = "PAYPAL";

  private final Jedis jedis;

  public JedisPaypalRepository(Jedis jedis) {
    this.jedis = jedis;
  }

  @Override
  public Optional<String> getPaypalEmail(OfflinePlayer player) {
    return Optional.ofNullable(jedis.hget(PAYPAL_KEY, hashKey(player)));
  }

  @Override
  public void setPaypalEmail(OfflinePlayer player, String email) {
    jedis.hset(PAYPAL_KEY, hashKey(player), email);
  }

  private static String hashKey(OfflinePlayer player) {
    return player.getUniqueId().toString();
  }
}
