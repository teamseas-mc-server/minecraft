package org.teamseas.minecraft.paypal;

import java.util.Optional;
import org.bukkit.OfflinePlayer;

/**
 * Data repository to store paypal information for a player.
 */
public interface PaypalRepository {
  
  public Optional<String> getPaypalEmail(OfflinePlayer player);

  public void setPaypalEmail(OfflinePlayer player, String email);
}
