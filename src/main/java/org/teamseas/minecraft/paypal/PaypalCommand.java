package org.teamseas.minecraft.paypal;

import java.util.Optional;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Command that shows and sets player's paypal email.
 */
public class PaypalCommand implements CommandExecutor {

  private final PaypalRepository repository;

  public PaypalCommand(PaypalRepository repository) {
    this.repository = repository;
  }

  @Override
  public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    OfflinePlayer player = (Player) sender;

    if (args.length == 0) {
      Optional<String> email = repository.getPaypalEmail(player);

      if (email.isPresent()) {
        sender.sendMessage(ChatColor.GREEN + "Your paypal email is " + email.get() + ".");
      } else {
        sender.sendMessage(ChatColor.RED + "You have not set your paypal email.");
      }
    } else {
      String email = args[0];

      repository.setPaypalEmail(player, email);
      sender.sendMessage(ChatColor.GREEN + "Your paypal email has been set to " + email + ".");
    }

    return true;
  }
}
