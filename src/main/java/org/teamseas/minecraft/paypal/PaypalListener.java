package org.teamseas.minecraft.paypal;

import java.util.function.Predicate;
import java.util.stream.Stream;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.teamseas.minecraft.Plugin;
import org.teamseas.minecraft.contest.Contest;
import org.teamseas.minecraft.contest.Winner;
import org.teamseas.minecraft.contest.WinnerRepository;

/**
 * Listener that notifies winners about missing paypal.
 */
public class PaypalListener implements Listener {

  private final Plugin plugin;
  private final PaypalRepository paypalRepository;
  private final WinnerRepository winnerRepository;

  /**
   * Creates an event listener. 
   */
  public PaypalListener(Plugin plugin, PaypalRepository paypalRepository,
      WinnerRepository winnerRepository) {
    this.plugin = plugin;
    this.paypalRepository = paypalRepository;
    this.winnerRepository = winnerRepository;
  }

  /**
   * Notifies about missing paypal when a winner joins a server.
   */
  @EventHandler
  public void onPlayerJoin(PlayerJoinEvent event) {
    Player player = event.getPlayer();
    if (paypalRepository.getPaypalEmail(player).isPresent()) {
      return;
    }
    Predicate<Contest> isContestWinner = contest -> winnerRepository.listWinners(contest).stream()
        .map(Winner::getPlayer).anyMatch(p -> p.equals(player));
    boolean isWinner = Stream.of(Contest.values()).anyMatch(isContestWinner);
    if (isWinner) {
      String message = "&6Congratulations! You have won a contest! "
          + "We need a way of sending you the prize money. "
          + "So please add your PayPal email address with /paypal <email>.";
      deferred(() -> player.sendMessage(color(message)));
    }
  }
  
  private void deferred(Runnable task) {
    int delayInSeconds = 15;
    plugin.getServer().getScheduler().runTaskLater(plugin, task, delayInSeconds * 20);
  }

  private static String color(String text) {
    return ChatColor.translateAlternateColorCodes('&', text);
  }
}
