FROM openjdk:17

WORKDIR /opt/minecraft
COPY .  /opt/minecraft/

ADD https://airplane.gg/dl/launcher-airplane1.17.1.jar core.jar
ADD https://github.com/Tiiffi/mcrcon/releases/download/v0.7.2/mcrcon-0.7.2-linux-x86-64.tar.gz /usr/local/bin
RUN cd /usr/local/bin && tar -zxf mcrcon-0.7.2-linux-x86-64.tar.gz

EXPOSE 25565
EXPOSE 25567
EXPOSE 25575

ENV AIKAR_FLAGS " \
  -XX:+UseG1GC \
  -XX:+ParallelRefProcEnabled \
  -XX:MaxGCPauseMillis=200 \
  -XX:+UnlockExperimentalVMOptions \
  -XX:+DisableExplicitGC \
  -XX:+AlwaysPreTouch \
  -XX:G1NewSizePercent=30 \
  -XX:G1MaxNewSizePercent=40 \
  -XX:G1HeapRegionSize=8M \
  -XX:G1ReservePercent=20 \
  -XX:G1HeapWastePercent=5 \
  -XX:G1MixedGCCountTarget=4 \
  -XX:InitiatingHeapOccupancyPercent=15 \
  -XX:G1MixedGCLiveThresholdPercent=90 \
  -XX:G1RSetUpdatingPauseTimePercent=5 \
  -XX:SurvivorRatio=32 \
  -XX:+PerfDisableSharedMem \
  -XX:MaxTenuringThreshold=1 \
  -Daikars.new.flags=true \
  -Dusing.aikars.flags=https://mcflags.emc.gs \
  "

CMD ["/opt/minecraft/start.sh"]
